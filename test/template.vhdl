library IEEE;
use IEEE.std_logic_1164.all;

library vunit_lib;
context vunit_lib.vunit_context;

library rtl_lib;

entity tb_template is
  generic (runner_cfg : string);
end entity;

architecture tb of tb_template is
begin
  main : process
  begin
    test_runner_setup(runner, runner_cfg);

    tests: while test_suite loop
      if run("Initial empty test") then
        check_passed;
      end if;

    end loop tests;

    test_runner_cleanup(runner);
  end process;

end architecture;
