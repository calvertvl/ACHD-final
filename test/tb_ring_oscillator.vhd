library IEEE;
use IEEE.std_logic_1164.all;

library vunit_lib;
context vunit_lib.vunit_context;

library rtl_lib;

entity tb_ring_oscillator is
  generic (runner_cfg : string);
end entity;

architecture tb of tb_ring_oscillator is
  -- Clock period of 1kHz should be fine for testing
  constant clk_period : integer := 1e6; -- ns
  constant check_delay : time := (clk_period/100) * 1 ns;
  constant states : positive := 8;
  signal clk : std_logic := '0';
  signal mstclr_n : std_logic := '1';
  signal end_inst : std_logic := '0';
  signal T : std_logic_vector(states-1 downto 0);
begin
  main : process
  begin
    test_runner_setup(runner, runner_cfg);

    tests: while test_suite loop
      mstclr_n <= '0';
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      mstclr_n <= '1';
      if run("Confirm initial state") then
        check_one_hot(T);
        log(to_string(T));
        check(T(0) = '1',"Expected bit 0 to be high.");
      elsif run("Check sequencing") then
        for i in 0 to states-1 loop
          log(to_string(T));
          check(T(i)='1',"Expected bit " & to_string(i) & " to be high");
          wait until rising_edge(clk);
          wait for check_delay;
        end loop;
      elsif run("Check wraparound") then
        for i in 0 to states-1 loop
          wait until rising_edge(clk);
        end loop;
        wait for check_delay;
        check(T(0) = '1',"Expected bit 0 to be high.");
      elsif run("Check reset") then
        wait until rising_edge(clk);
        end_inst <= '1';
        wait until rising_edge(clk);
        wait for check_delay;
        check(T(0) = '1',"Expected bit 0 to be high.");
      end if;

    end loop tests;

    test_runner_cleanup(runner);
  end process;

  clk <= not clk after (clk_period/2)*1 ns;

  dut : entity rtl_lib.ring_oscillator
    generic map (
      states => states)
    port map (
      clk => clk,
      mstclr_n => mstclr_n,
      end_inst => end_inst,
      T => T);
end architecture;
