from os.path import join, dirname
from vunit import VUnit

root = dirname(__file__)

rtl = join(root,"rtl/")
test = join(root,"test/")

vu = VUnit.from_argv()

rtl_lib = vu.add_library("rtl_lib")
test_lib = vu.add_library("test_lib")

rtl_lib.add_source_files(join(rtl,"*.vhd"),allow_empty=True)
test_lib.add_source_files(join(test,"*.vhd"),allow_empty=True)

vu.main()

