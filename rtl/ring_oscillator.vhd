library IEEE;
use IEEE.std_logic_1164.all;
-- use IEEE.numeric_std.all;

entity ring_oscillator is
  generic (
    states   : positive
    ) ;
  port (
    clk      : in  std_logic;
    mstclr_n : in  std_logic;
    end_inst : in  std_logic;
    T        : out std_logic_vector(states-1 downto 0)
    );
end ring_oscillator;

architecture ring_arch of ring_oscillator is
begin
  main : process(clk) is
  begin
    if rising_edge(clk) then
      if mstclr_n = '0' or end_inst = '1' then
        for i in T'range loop
          T(i) <= '1' when i = 0 else '0';
        end loop;
      elsif end_inst = '0' then
        T <= T rol 1; -- rotate left 1 bit
      end if;
    end if;
  end process main;
end ring_arch;
