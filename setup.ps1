$validPolicies = 'RemoteSigned','Bypass','AllSigned'
if ($validPolicies -contains (Get-ExecutionPolicy))
{
    echo "yay"
}
else {
    echo "You need to run the following command:"
    echo "Set_ExecutionPolicy RemoteSigned"
}

if (-not (Get-Command git -errorAction SilentlyContinue)) {
    if (-not (Get-Command choco -errorAction SilentlyContinue)) {
        echo "Installing chocolatey..."
        iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
    }
    choco install git -y
}

if (-not (Get-Command python -errorAction SilentlyContinue)) {
    if (-not (Get-Command choco -errorAction SilentlyContinue)) {
        echo "Installing chocolatey..."
        iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
    }
    choco install python -y
}

if (-not (Test-Path C:\tools)) {
    mkdir c:\tools
}

if (-not (Get-Command C:\tools\GHDL\lib\vendors\compile-altera.ps1 -errorAction SilentlyContinue)) {
    .\ghdl\ghdl-0.34-dev-mcode-2017-03-01-win32.installer.ps1 -Install "C:\tools\GHDL\"
    cp ghdl\lib\vendor\*.* C:\tools\GHDL\lib\vendors\
}

if (-not (Test-Path C:\tools\vunit\.git)) {
    pushd C:\tools\
    git clone https://github.com/VUnit/vunit.git
    popd
}

.\precompile.ps1
